package TCP;
import java.io.*;
import java.net.Socket;
import java.util.Scanner;
public class ClienteTCP extends Thread{
    @Override
    public void run(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Creando y conectando el socket stream cliente");
        String [] operandos= {"0","1","2","3","4","5","6","7","8","9","salir"};
        String [] operadores = {"+","-","*","/"};
        try {
            Socket cliente = new Socket("localhost",50005 );

            //Stream de entrada de datos
            InputStream is = cliente.getInputStream();
            DataInputStream dis = new DataInputStream(is);
            //Stream de salida de datos
            OutputStream os = cliente.getOutputStream();
            DataOutputStream dos = new DataOutputStream(os);

            //operando 1
            System.out.println(dis.readUTF());
            int respuesta1 = (int) (Math.random() * 11);
            dos.writeUTF(operandos[respuesta1]);
            if (!operandos[respuesta1].equals(operandos[10])){
                //opreando 2
                System.out.println(dis.readUTF());
                int respuesta2 = (int) (Math.random() * 10);
                dos.writeUTF(operandos[respuesta2]);

                //operador
                System.out.println(dis.readUTF());
                int operador = (int) (Math.random() * 4);
                dos.writeUTF(operadores[operador]);

                //resultado
                System.out.println(dis.read());
            }

            System.out.println("Cerrando conexión con el cliente...");
            dos.close();
            dis.close();
            cliente.close();
            System.out.println("Terminando");
        } catch (IOException e) {
            System.err.println("Error al crear el socket.");
            System.out.println(e.getMessage());
        }
    }
    public static void main(String[] args) {
        ClienteTCP cliente1 = new ClienteTCP();
        ClienteTCP cliente2 = new ClienteTCP();
        ClienteTCP cliente3 = new ClienteTCP();
        ClienteTCP cliente4 = new ClienteTCP();
        ClienteTCP cliente5 = new ClienteTCP();
        ClienteTCP cliente6 = new ClienteTCP();
        ClienteTCP cliente7 = new ClienteTCP();
        ClienteTCP cliente8 = new ClienteTCP();

        cliente1.start();
        cliente2.start();
        cliente3.start();
        cliente4.start();
        cliente5.start();
        cliente6.start();
        cliente7.start();
        cliente8.start();
    }
}
