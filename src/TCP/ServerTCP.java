package TCP;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Semaphore;
public class ServerTCP {
    public static void main(String[] args) {
        Semaphore semaforo = new Semaphore(5);
        try{
            //construir el objeto para que escuche por el puerto indicado
            ServerSocket servidor = new ServerSocket(50005);

            while (true){
                System.out.println("Aceptando conexiones...");
                //Espera bloqueado conexiones de clientes
                //Cuando llegue una devolverá el accept un Socket
                Socket cliente = servidor.accept();
                System.out.println("Conexión establecida!");
                //inicialización de los streams para entreda y salida de datos
                DataInputStream entrada = new DataInputStream(cliente.getInputStream());
                DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());
                String respuesta1 = "";
                int respuesta2 = 0;

                semaforo.acquire();
                //operando 1
                salida.writeUTF("Dime el primer operando o salir");
                respuesta1 = entrada.readUTF();
                System.out.println(respuesta1);

                //operando 2
                salida.writeUTF("Dime el segundo operando");
                respuesta2 = Integer.parseInt(entrada.readUTF());
                System.out.println(respuesta2);

                //operador
                salida.writeUTF("Dime el operador\n" +
                        "1. +\n" +
                        "2. -\n" +
                        "3. *\n" +
                        "4. /");
                String operador = entrada.readUTF();
                System.out.println(operador);
                switch (operador){
                    case "+":
                        double solucion = Integer.parseInt(respuesta1) + respuesta2;
                        salida.writeUTF(String.valueOf(solucion));
                        System.out.println(solucion);
                        break;
                    case "-":
                        solucion = Integer.parseInt(respuesta1) - respuesta2;
                        salida.writeUTF(String.valueOf(solucion));
                        System.out.println(solucion);
                        break;
                    case "*":
                        solucion = Integer.parseInt(respuesta1) * respuesta2;
                        salida.writeUTF(String.valueOf(solucion));
                        System.out.println(solucion);
                        break;
                    case "/":
                        solucion = (double) Integer.parseInt(respuesta1) / respuesta2;
                        salida.writeUTF(String.valueOf(solucion));
                        System.out.println(solucion);
                        break;
                }
                semaforo.release();

                entrada.close();
                salida.close();
                cliente.close();
            }

            //System.out.println("Cerrando Streams y sockets...");
            //servidor.close();
        }catch (IOException e){
            System.err.println("Error al crear el socket");
            System.out.println(e.getMessage());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
