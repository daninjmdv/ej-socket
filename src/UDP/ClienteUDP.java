package UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ClienteUDP extends Thread{
    @Override
    public void run(){
        try {
            DatagramSocket datagramSocket = new DatagramSocket();
            InetAddress serverAddr = InetAddress.getByName("localhost");
            String mensaje = "Cliente conectado";
            //construimos el paquete
            DatagramPacket paquete = new DatagramPacket(mensaje.getBytes(), mensaje.length(), serverAddr, 5555);
            datagramSocket.send(paquete);
            System.out.println("menesaje enviado");

            //operando 1
            byte[] respuesta = new byte[1024];
            paquete = new DatagramPacket(respuesta, respuesta.length);
            datagramSocket.receive(paquete);
            System.out.println(new String(respuesta).trim());

            String operando1 = String.valueOf((int) (Math.random() * 10) + 1);
            paquete = new DatagramPacket(operando1.getBytes(), operando1.length(), serverAddr, 5555);
            datagramSocket.send(paquete);

            //operando 2
            respuesta = new byte[1024];
            paquete = new DatagramPacket(respuesta, respuesta.length);
            datagramSocket.receive(paquete);
            System.out.println(new String(respuesta).trim());

            String operando2 = String.valueOf((int) (Math.random() * 10) + 1);
            paquete = new DatagramPacket(operando2.getBytes(), operando2.length(), serverAddr, 5555);
            datagramSocket.send(paquete);

            //operador
            respuesta = new byte[1024];
            paquete = new DatagramPacket(respuesta, respuesta.length);
            datagramSocket.receive(paquete);
            System.out.println(new String(respuesta).trim());

            String[] operadores = {"+", "-", "*", "/"};
            int operador = ((int) (Math.random() * 4));
            paquete = new DatagramPacket(operadores[operador].getBytes(), operadores[operador].length(), serverAddr, 5555);
            datagramSocket.send(paquete);

            //solución
            respuesta = new byte[1024];
            paquete = new DatagramPacket(respuesta, respuesta.length);
            datagramSocket.receive(paquete);
            System.out.println(new String(respuesta).trim());

            datagramSocket.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    public static void main(String[] args) {
        ClienteUDP cliente1 = new ClienteUDP();
        ClienteUDP cliente2 = new ClienteUDP();
        ClienteUDP cliente3 = new ClienteUDP();
        ClienteUDP cliente4 = new ClienteUDP();
        ClienteUDP cliente5 = new ClienteUDP();

        cliente1.start();
        cliente2.start();
        cliente3.start();
        cliente4.start();
        cliente5.start();

    }
}
