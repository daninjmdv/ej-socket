package UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;

public class ServerUDP {
    public static void main(String[] args) {

        try {
            InetSocketAddress addr = new InetSocketAddress("localhost", 5555);
            DatagramSocket socket = new DatagramSocket(addr);
            System.out.println("Servidor conectado");

            //Recepción de mensajes
            DatagramPacket paquete;
            while(true) {
                paquete = new DatagramPacket(new byte[1024], 1024);
                //Espera bloqueante mientras recibo la información

                socket.receive(paquete);
                String mensaje = new String(paquete.getData()).trim();
                System.out.println(mensaje);
                InetAddress ip_cliente = paquete.getAddress();
                int puerto_cliente = paquete.getPort();

                //operando 1
                String mensaje_servidor = "Dime el primer operando";
                paquete = new DatagramPacket(mensaje_servidor.getBytes(), mensaje_servidor.length(), ip_cliente, puerto_cliente);
                socket.send(paquete);

                paquete = new DatagramPacket(new byte[1024], 1024);
                socket.receive(paquete);
                mensaje = new String(paquete.getData()).trim();
                System.out.println(mensaje);
                String respuesta1 = mensaje;

                //operando 2
                mensaje_servidor = "Dime el segundo operando";
                paquete = new DatagramPacket(mensaje_servidor.getBytes(), mensaje_servidor.length(), ip_cliente, puerto_cliente);
                socket.send(paquete);

                paquete = new DatagramPacket(new byte[1024], 1024);
                socket.receive(paquete);
                mensaje = new String(paquete.getData()).trim();
                System.out.println(mensaje);
                String respuesta2 = mensaje;

                //operador
                mensaje_servidor = "Dime el operador\n" +
                        "1. +\n" +
                        "2. -\n" +
                        "3. *\n" +
                        "4. /";
                paquete = new DatagramPacket(mensaje_servidor.getBytes(), mensaje_servidor.length(), ip_cliente, puerto_cliente);
                socket.send(paquete);

                paquete = new DatagramPacket(new byte[1024], 1024);
                socket.receive(paquete);
                mensaje = new String(paquete.getData()).trim();
                System.out.println(mensaje);
                String operador = mensaje;

                String solucion;
                if (operador.equals("+")) {
                    solucion = String.valueOf(Integer.parseInt(respuesta1) + Integer.parseInt(respuesta2));
                    paquete = new DatagramPacket(solucion.getBytes(), solucion.length(), ip_cliente, puerto_cliente);
                    socket.send(paquete);
                } else if (operador.equals("-")) {
                    solucion = String.valueOf(Integer.parseInt(respuesta1) - Integer.parseInt(respuesta2));
                    paquete = new DatagramPacket(solucion.getBytes(), solucion.length(), ip_cliente, puerto_cliente);
                    socket.send(paquete);
                } else if (operador.equals("*")) {
                    solucion = String.valueOf(Integer.parseInt(respuesta1) * Integer.parseInt(respuesta2));
                    paquete = new DatagramPacket(solucion.getBytes(), solucion.length(), ip_cliente, puerto_cliente);
                    socket.send(paquete);
                } else if (operador.equals("/")) {
                    solucion = String.valueOf(Integer.parseInt(respuesta1) / Integer.parseInt(respuesta2));
                    paquete = new DatagramPacket(solucion.getBytes(), solucion.length(), ip_cliente, puerto_cliente);
                    socket.send(paquete);
                }


            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
}
